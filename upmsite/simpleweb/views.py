from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from simpleweb import models
from .decorators import unauthenticated_user, allowed_users
from django.contrib.auth.decorators import login_required
from simpleweb.forms import LinkFileForm, LinkFolderForm, LinkFileFormABPT, LinkFolderFormABPT, LinkFolderFormBM, LinkFolderFormFBT, LinkFolderFormREE, LinkFolderFormPDE, LinkFolderFormCSE
from django.views.generic.edit import CreateView, UpdateView
from django.urls import resolve

@unauthenticated_user
def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            member_type = request.user.groups.all()
            print(member_type)
            return redirect("home", member_type[0])
        else:
            messages.info(request, 'Username or password is incorrect')

    context = {}
    return render(request,'UPM_Login.html', context)

def logoutUser(request):
    logout(request)
    return redirect("login")

# _____________________________________ ADMIN _____________________________________

@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def homeadmin(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)

    context = {'member_type': member_type,
               'judul': 'Home',
               'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
               'link': 'admin',}
    return render(request,'UPM_Home.html',context)

url_next = ''
url_prodi = ''
@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def amiprodiadmin(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    
    semua_folderamiprodi = models.FolderAmiProdi.objects.all()
    semua_folderamiprodibm = models.FolderAmiProdiBM.objects.all()
    semua_folderamiprodifbt = models.FolderAmiProdiFBT.objects.all()
    semua_folderamiprodipde = models.FolderAmiProdiPDE.objects.all()
    semua_folderamiprodiree = models.FolderAmiProdiREE.objects.all()
    semua_folderamiprodicse = models.FolderAmiProdiCSE.objects.all()

    global url_prodi, url_next
    url_prodi = resolve(request.path_info).url_name
    url_next = resolve(request.path_info).url_name

    if pk_test == 'admin':
        if url_next == "ami prodi dbt":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Prodi Digital Business Technology',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodi,
        })
        elif url_next == "ami prodi bm":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Business Mathematics',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodibm,
        })
        elif url_next == "ami prodi fbt":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Food Business Technology',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodifbt,
        })
        elif url_next == "ami prodi pde":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Product Design Engineering',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodipde,
        })
        elif url_next == "ami prodi ree":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Renewable Energy Engineering',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodiree,
        })
        elif url_next == "ami prodi cse":
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Computer System Engineering',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodicse,
        })
        else:
            return render(request,'UPM_Prodi_Admin.html',{
                'member_type': member_type,
                'judul': 'Prodi Digital Business Technology',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderamiprodi,
        })
            
    elif pk_test == 'DBT':
        return render(request,'UPM_Prodi_NotAdmin.html',{
            'member_type': member_type,
            'judul': 'Prodi Digital Business Technology',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderamiprodi,
    })
    elif pk_test == 'BM':
        return render(request,'UPM_Prodi_NotAdmin.html',{
            'member_type': member_type,
            'judul': 'Business Mathematics',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderamiprodibm,
    })
def prodi_url():
    return url_prodi
def next_url():
    return url_next


url_umum = ''
@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def amiumumadmin(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    semua_linkfileamiumum = models.LinkfileAmiUmum.objects.all()
    global url_umum
    url_umum = resolve(request.path_info).url_name

    if pk_test == 'admin':
        return render(request, 'UPM_Umum_Admin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileamiumum,
    })
    elif pk_test == 'DBT':
        return render(request, 'UPM_Umum_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileamiumum,
    })
    elif pk_test == 'BM':
        return render(request, 'UPM_Umum_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileamiumum,
    })
def umum_url():
    return url_umum


@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def abptprodiadmin(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    semua_folderabptprodi = models.FolderAbptProdi.objects.all()
    semua_folderabptprodibm = models.FolderAbptProdiBM.objects.all()
    semua_folderabptprodifbt = models.FolderAbptProdiFBT.objects.all()
    semua_folderabptprodipde = models.FolderAbptProdiPDE.objects.all()
    semua_folderabptprodiree = models.FolderAbptProdiREE.objects.all()
    semua_folderabptprodicse = models.FolderAbptProdiCSE.objects.all()
    
    global url_prodi, url_next
    url_prodi = resolve(request.path_info).url_name
    url_next = resolve(request.path_info).url_name

    if pk_test == 'admin':
        if url_next == "abpt prodi dbt":
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Prodi Digital Business Technology',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodi,
            })
        elif url_next == "abpt prodi bm":
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Business Mathematics',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodibm,
            })
        elif url_next == "abpt prodi fbt":
            return render(request, 'UPM_Prodi_NotAdmin.html', {
                'member_type': member_type,
                'judul': 'Food Business Technology',
                'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                'link': 'admin',
                'file_folder': semua_folderabptprodifbt,
            })
        elif url_next == "abpt prodi pde":
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Product Design Engineering',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodipde,
            })
        elif url_next == "abpt prodi ree":
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Renewable Energy Engineering',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodiree,
            })
        elif url_next == "abpt prodi cse":
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Computer System Engineering',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodicse,
            })
        else:
            return render(request, 'UPM_Prodi_Admin_ABPT.html', {
                    'member_type': member_type,
                    'judul': 'Prodi Digital Business Technology',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
                    'link': 'admin',
                    'file_folder': semua_folderabptprodi,
            })
    elif pk_test == 'DBT':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Digital Business Technology',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodi,
    })
    elif pk_test == 'BM':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Business Mathematics',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodibm,
    })
    elif pk_test == 'FBT':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Food Business Technology',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodifbt,
    })
    elif pk_test == 'PDE':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Product Design Engineering',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodipde,
    })
    elif pk_test == 'REE':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Renewable Energy Engineering',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodiree,
    })
    elif pk_test == 'CSE':
        return render(request, 'UPM_Prodi_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Computer System Engineering',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_folderabptprodicse,
    })
def prodi_url():
    return url_prodi
def next_url():
    return url_next


@login_required(login_url='login')
#@allowed_users(allowed_roles=['admin'])
def abptumumadmin(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    semua_linkfileabptumum = models.LinkfileAbptUmum.objects.all()
    global url_umum
    url_umum = resolve(request.path_info).url_name

    if pk_test == 'admin':
        return render(request, 'UPM_Umum_Admin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileabptumum,
    })
    elif pk_test == 'DBT':
        return render(request, 'UPM_Umum_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileabptumum,
    })
    elif pk_test == 'BM':
        return render(request, 'UPM_Umum_NotAdmin.html', {
            'member_type': member_type,
            'judul': 'Umum',
            'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam volutpat tellus nec suscipit tempus. Maecenas quis sem nec leo dignissim vehicula ac in eros. In hac habitasse platea dictumst. Nulla eu felis ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellu dictum placerat justo quis lobortis. Etiam vestibulum aliquet elit, non vehicula augue.',
            'link': 'admin',
            'file_folder': semua_linkfileabptumum,
    })
def umum_url():
    return url_umum

# _____________________________________ LAINNYA _____________________________________

@login_required(login_url='login')
def informasiumum(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    semua_folderinformasiumum = models.FolderInformasiUmum.objects.all()

    return render(request, 'UPM_Informasi_Umum.html', {
        'member_type': member_type,
        'file_folder': semua_folderinformasiumum,
        'link': 'admin',
    })

def createFile(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    form = LinkFileForm()
    form2 = LinkFileFormABPT()
    current_url_umum = umum_url()
    
    
    if current_url_umum == "ami umum admin":
        if request.method == 'POST':
            #print('Printing POST:', request.POST)
            form = LinkFileForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('ami umum admin', member_type)

        if pk_test == 'admin':
            context = {'member_type': member_type,
                    'pathloc': 'update file',
                    'form': form,
                    'judul': 'Create File AMI UMUM DBT',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_File.html', context)
        
    
    elif current_url_umum == "abpt umum admin":
        if request.method == 'POST':
            #print('Printing POST:', request.POST)
            form2 = LinkFileFormABPT(request.POST)
            if form2.is_valid():
                form2.save()
                return redirect('abpt umum admin', member_type)

        if pk_test == 'admin':
            context = {'member_type': member_type, 
                    'form': form2,
                    'judul': 'Create File ABPT UMUM DBT',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_File.html', context)

def updateFile(request, pk_test, pk):
    member_type = request.user.groups.get(name=pk_test)
    current_url_umum2 = umum_url()


    if current_url_umum2 == "ami umum admin":
        daftar_data = models.LinkfileAmiUmum.objects.get(id=pk)
        form = LinkFileForm(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFileForm(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami umum admin', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update File AMI UMUM DBT',}
        return render(request, 'UPM_Create-Update_File.html', context)
    elif current_url_umum2 == "abpt umum admin":
        daftar_data2 = models.LinkfileAbptUmum.objects.get(id=pk)
        form2 = LinkFileFormABPT(instance=daftar_data2)
        if request.method == 'POST':
            form2 = LinkFileFormABPT(request.POST, instance=daftar_data2)
            if form2.is_valid():
                form2.save()
                return redirect('abpt umum admin', member_type)
        
        context = {'form': form2,
                'member_type': member_type,
                'judul': 'Update File ABPT UMUM DBT',}
        return render(request, 'UPM_Create-Update_File.html', context)

def deleteFile(request, pk_test, pk):
    member_type = request.user.groups.get(name=pk_test)
    current_url_umum3 = umum_url()

    if current_url_umum3 == "ami umum admin":
        daftar_data = models.LinkfileAmiUmum.objects.get(id=pk)
        if request.method == "POST":
            daftar_data.delete()
            return redirect('ami umum admin', member_type)

        context = {'item': daftar_data,
                'member_type': member_type,
                'judul': 'Delete File AMI UMUM DBT',}
        return render(request, "UPM_Delete_File.html", context)
    elif current_url_umum3 == "abpt umum admin":
        daftar_data2 = models.LinkfileAbptUmum.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('abpt umum admin', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete File ABPT UMUM DBT',}
        return render(request, "UPM_Delete_File.html", context)

def createFolder(request, pk_test):
    member_type = request.user.groups.get(name=pk_test)
    # current_url_prodi = prodi_url()
    next_url_prodi = next_url()

    if pk_test == "admin":
        # if current_url_prodi == "ami prodi admin":
        if next_url_prodi == "ami prodi dbt":
            form = LinkFolderForm()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderForm(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi dbt', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI DBT',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        elif next_url_prodi == "ami prodi bm":
            form = LinkFolderFormBM()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderFormBM(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi bm', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI BM',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        elif next_url_prodi == "ami prodi fbt":
            form = LinkFolderFormFBT()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderFormFBT(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi fbt', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI FBT',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        elif next_url_prodi == "ami prodi pde":
            form = LinkFolderFormPDE()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderFormPDE(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi pde', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI PDE',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        elif next_url_prodi == "ami prodi ree":
            form = LinkFolderFormREE()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderFormREE(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi ree', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI REE',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        elif next_url_prodi == "ami prodi cse":
            form = LinkFolderFormCSE()
            if request.method == 'POST':
                #print('Printing POST:', request.POST)
                form = LinkFolderFormCSE(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('ami prodi cse', member_type)

            context = {'member_type': member_type, 
                    'form': form,
                    'judul': 'Create Folder AMI PRODI CSE',
                    'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
            return render(request, 'UPM_Create-Update_Folder.html', context)
        

        # elif current_url_prodi == "abpt prodi admin":
        #     form2 = LinkFolderFormABPT()
        #     if request.method == 'POST':
        #         #print('Printing POST:', request.POST)
        #         form2 = LinkFolderFormABPT(request.POST)
        #         if form2.is_valid():
        #             form2.save()
        #             return redirect('abpt prodi admin', member_type)

        #     context = {'member_type': member_type, 
        #             'form': form2,
        #             'judul': 'Create Folder ABPT PRODI DBT',
        #             'deskripsi': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac purus magna. Aliquam condimentum quam quis quam fermentum, vel tempus diam blandit. Maecenas eleifend faucibus elit, in placerat dolor tincidunt at. Phasellus luctus pellentesque facilisis. Ut faucibus rhoncus metus.Maecenas pharetra, sapien sit amet vestibulum posuere, nisi ante facilisis ex, nec scelerisque sem diam semper felis.',}
        #     return render(request, 'UPM_Create-Update_Folder.html', context)


def updateFolder(request, pk_test, pk):
    member_type = request.user.groups.get(name=pk_test)
    # current_url_prodi2 = prodi_url()
    next_url_prodi2 = next_url()
    
    # if current_url_prodi2 == "ami prodi admin":
    if next_url_prodi2 == "ami prodi dbt":
        daftar_data = models.FolderAmiProdi.objects.get(id=pk)
        form = LinkFolderForm(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderForm(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi dbt', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM DBT',}
        return render(request, 'UPM_Create-Update_Folder.html', context)
    elif next_url_prodi2 == "ami prodi bm":
        daftar_data = models.FolderAmiProdiBM.objects.get(id=pk)
        form = LinkFolderFormBM(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderFormBM(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi bm', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM BM',}
        return render(request, 'UPM_Create-Update_Folder.html', context)
    elif next_url_prodi2 == "ami prodi fbt":
        daftar_data = models.FolderAmiProdiFBT.objects.get(id=pk)
        form = LinkFolderFormFBT(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderFormFBT(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi fbt', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM FBT',}
        return render(request, 'UPM_Create-Update_Folder.html', context)
    elif next_url_prodi2 == "ami prodi pde":
        daftar_data = models.FolderAmiProdiPDE.objects.get(id=pk)
        form = LinkFolderFormPDE(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderFormPDE(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi pde', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM PDE',}
        return render(request, 'UPM_Create-Update_Folder.html', context)
    elif next_url_prodi2 == "ami prodi ree":
        daftar_data = models.FolderAmiProdiREE.objects.get(id=pk)
        form = LinkFolderFormREE(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderFormREE(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi ree', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM REE',}
        return render(request, 'UPM_Create-Update_Folder.html', context)
    elif next_url_prodi2 == "ami prodi cse":
        daftar_data = models.FolderAmiProdiCSE.objects.get(id=pk)
        form = LinkFolderFormCSE(instance=daftar_data)
        if request.method == 'POST':
            form = LinkFolderFormCSE(request.POST, instance=daftar_data)
            if form.is_valid():
                form.save()
                return redirect('ami prodi cse', member_type)
        
        context = {'form': form,
                'member_type': member_type,
                'judul': 'Update Folder AMI UMUM cse',}
        return render(request, 'UPM_Create-Update_Folder.html', context)

    # elif current_url_prodi2 == "abpt prodi admin":
    #     daftar_data2 = models.FolderAbptProdi.objects.get(id=pk)
    #     form2 = LinkFolderFormABPT(instance=daftar_data2)
    #     if request.method == 'POST':
    #         form2 = LinkFolderFormABPT(request.POST, instance=daftar_data2)
    #         if form2.is_valid():
    #             form2.save()
    #             return redirect('abpt prodi admin', member_type)
        
    #     context = {'form': form2,
    #             'member_type': member_type,
    #             'judul': 'Update Folder ABPT UMUM DBT',}
    #     return render(request, 'UPM_Create-Update_Folder.html', context)

def deleteFolder(request, pk_test, pk):
    member_type = request.user.groups.get(name=pk_test)
    # current_url_prodi3 = prodi_url()
    next_url_prodi3 = next_url()

    if next_url_prodi3 == "ami prodi dbt":
        daftar_data = models.FolderAmiProdi.objects.get(id=pk)
        if request.method == "POST":
            daftar_data.delete()
            return redirect('ami prodi dbt', member_type)

        context = {'item': daftar_data,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI DBT',}
        return render(request, "UPM_Delete_Folder.html", context)
    elif next_url_prodi3 == "ami prodi bm":
        daftar_data2 = models.FolderAmiProdiBM.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('ami prodi bm', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI BM',}
        return render(request, "UPM_Delete_Folder.html", context)
    elif next_url_prodi3 == "ami prodi fbt":
        daftar_data2 = models.FolderAmiProdiFBT.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('ami prodi fbt', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI FBT',}
        return render(request, "UPM_Delete_Folder.html", context)
    elif next_url_prodi3 == "ami prodi pde":
        daftar_data2 = models.FolderAmiProdiPDE.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('ami prodi pde', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI PDE',}
        return render(request, "UPM_Delete_Folder.html", context)
    elif next_url_prodi3 == "ami prodi ree":
        daftar_data2 = models.FolderAmiProdiREE.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('ami prodi ree', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI REE',}
        return render(request, "UPM_Delete_Folder.html", context)
    elif next_url_prodi3 == "ami prodi cse":
        daftar_data2 = models.FolderAmiProdiCSE.objects.get(id=pk)
        if request.method == "POST":
            daftar_data2.delete()
            return redirect('ami prodi cse', member_type)

        context = {'item': daftar_data2,
                'member_type': member_type,
                'judul': 'Delete Folder AMI PRODI CSE',}
        return render(request, "UPM_Delete_Folder.html", context)



    