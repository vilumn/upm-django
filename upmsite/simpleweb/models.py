from django.db import models

# Create your models here.
class Major(models.Model):
    major_option = (
        ('Software Engineering', 'SE'),
        ('Computer System Engineering', 'CSE'),
        ('Renewable Energy Engineering', 'REE'),
        ('Product Design Engineering', 'PDE'),
        ('Food Business Technology', 'FBT'),
        ('Business Mathematics', 'BM'),
    )
    name = models.CharField(choices=major_option, default=0, max_length=256)
    description = models.TextField(null=True, blank=True, max_length=512)

    def __str__(self):
        return '{}'.format(self.name)

class UserTypes(models.Model):
    user_type = (
        ('Faculty Member', 'FM'),
        ('Staff', 'Staff'),
    )
    member = models.CharField(choices=user_type, default=0, max_length=256)
    first_name = models.CharField(max_length=256, null=True)
    last_name = models.CharField(max_length=256, null=True)
    email = models.CharField(max_length=256, null=True)
    major = models.ForeignKey(Major, on_delete=models.CASCADE, default=0)

    def __str__(self):
        return '{} {} ({})'.format(self.first_name, self.last_name, self.major)

# ____________________ DIGITAL BUSINESS TECHNOLOGY ____________________

class FolderAmiProdi(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class LinkfileAmiUmum(models.Model):
    nama_file = models.CharField(max_length=80)
    link_file = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_file

class FolderAbptProdi(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class LinkfileAbptUmum(models.Model):
    nama_file = models.CharField(max_length=80)
    link_file = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_file

class FolderInformasiUmum(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

# ____________________ BUSINESS MATHEMATICS ____________________

class FolderAmiProdiBM(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class FolderAbptProdiBM(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

# ____________________ FOOD BUSINESS TECHNOLOGY ____________________

class FolderAmiProdiFBT(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class FolderAbptProdiFBT(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder


# ____________________ PRODUCT DESIGN ENGINEERING ____________________

class FolderAmiProdiPDE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class FolderAbptProdiPDE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

# ____________________ RENEWABLE ENERGY ENGINEERING ____________________

class FolderAmiProdiREE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class FolderAbptProdiREE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

# ____________________ COMPUTER SYSTEM ENGINEERING ____________________

class FolderAmiProdiCSE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder

class FolderAbptProdiCSE(models.Model):
    nama_folder = models.CharField(max_length=80)
    link_folder = models.CharField(max_length=256)

    def __str__(self):
        return self.nama_folder
