from django.contrib import admin
from .models import FolderAmiProdi
from .models import FolderAmiProdiBM
from .models import FolderAmiProdiFBT
from .models import FolderAmiProdiREE
from .models import FolderAmiProdiPDE
from .models import FolderAmiProdiCSE
from .models import LinkfileAmiUmum
from .models import FolderAbptProdi
from .models import LinkfileAbptUmum
from .models import FolderInformasiUmum
from .models import FolderAbptProdiBM
from .models import FolderAbptProdiFBT
from .models import FolderAbptProdiPDE
from .models import FolderAbptProdiREE
from .models import FolderAbptProdiCSE
from .models import Major
from .models import UserTypes

# Register your models here.
admin.site.register(FolderAmiProdi)
admin.site.register(FolderAmiProdiBM)
admin.site.register(FolderAmiProdiFBT)
admin.site.register(FolderAmiProdiREE)
admin.site.register(FolderAmiProdiPDE)
admin.site.register(FolderAmiProdiCSE)
admin.site.register(LinkfileAmiUmum)
admin.site.register(FolderAbptProdi)
admin.site.register(LinkfileAbptUmum)
admin.site.register(FolderInformasiUmum)
admin.site.register(FolderAbptProdiBM)
admin.site.register(FolderAbptProdiFBT)
admin.site.register(FolderAbptProdiPDE)
admin.site.register(FolderAbptProdiREE)
admin.site.register(FolderAbptProdiCSE)
admin.site.register(Major)
admin.site.register(UserTypes)
