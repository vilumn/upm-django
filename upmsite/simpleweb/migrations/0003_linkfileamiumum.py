# Generated by Django 3.1.2 on 2020-10-15 13:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('simpleweb', '0002_auto_20201015_1922'),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkfileAmiUmum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_file', models.CharField(max_length=80)),
                ('link_file', models.CharField(max_length=120)),
            ],
        ),
    ]
