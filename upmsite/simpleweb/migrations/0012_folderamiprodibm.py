# Generated by Django 3.1.2 on 2020-10-20 18:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('simpleweb', '0011_auto_20201020_2018'),
    ]

    operations = [
        migrations.CreateModel(
            name='FolderAmiProdiBM',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_folder', models.CharField(max_length=80)),
                ('link_folder', models.CharField(max_length=256)),
            ],
        ),
    ]
