from django.forms import ModelForm
from .models import LinkfileAmiUmum, FolderAmiProdi, FolderAmiProdiBM, FolderAmiProdiFBT, FolderAmiProdiPDE, FolderAmiProdiREE, FolderAmiProdiCSE, LinkfileAbptUmum, FolderAbptProdi

class LinkFileForm(ModelForm):
    class Meta:
        model = LinkfileAmiUmum
        fields = '__all__'

class LinkFolderForm(ModelForm):
    class Meta:
        model = FolderAmiProdi
        fields = '__all__'

class LinkFolderFormBM(ModelForm):
    class Meta:
        model = FolderAmiProdiBM
        fields = '__all__'

class LinkFolderFormFBT(ModelForm):
    class Meta:
        model = FolderAmiProdiFBT
        fields = '__all__'

class LinkFolderFormREE(ModelForm):
    class Meta:
        model = FolderAmiProdiREE
        fields = '__all__'

class LinkFolderFormPDE(ModelForm):
    class Meta:
        model = FolderAmiProdiPDE
        fields = '__all__'

class LinkFolderFormCSE(ModelForm):
    class Meta:
        model = FolderAmiProdiCSE
        fields = '__all__'



class LinkFileFormABPT(ModelForm):
    class Meta:
        model = LinkfileAbptUmum
        fields = '__all__'

class LinkFolderFormABPT(ModelForm):
    class Meta:
        model = FolderAbptProdi
        fields = '__all__'

