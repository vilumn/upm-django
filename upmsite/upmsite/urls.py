from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from simpleweb import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/',views.loginPage, name="login"),
    path('logout/',views.logoutUser, name="logout"),
    path('createfile/<str:pk_test>/',views.createFile, name="create file"),
    path('updatefile/<str:pk_test>/<str:pk>',views.updateFile, name="update file"),
    path('deletefile/<str:pk_test>/<str:pk>',views.deleteFile, name="delete file"),
    path('createfolder/<str:pk_test>/',views.createFolder, name="create folder"),
    path('updatefolder/<str:pk_test>/<str:pk>',views.updateFolder, name="update folder"),
    path('deletefolder/<str:pk_test>/<str:pk>',views.deleteFolder, name="delete folder"),

    # ____________ ADMIN ____________

    path('home/<str:pk_test>/',views.homeadmin, name="home"),
    path('ami/prodi/<str:pk_test>/',views.amiprodiadmin, name="ami prodi admin"),
    path('ami/umum/<str:pk_test>/',views.amiumumadmin, name="ami umum admin"),
    path('abpt/prodi/<str:pk_test>/',views.abptprodiadmin, name="abpt prodi admin"),
    path('abpt/umum/<str:pk_test>/',views.abptumumadmin, name="abpt umum admin"),
    path('informasi_umum/<str:pk_test>/',views.informasiumum, name="informasi umum"),

    path('ami/prodi/<str:pk_test>/DBT/',views.amiprodiadmin, name="ami prodi dbt"),
    path('ami/prodi/<str:pk_test>/BM/',views.amiprodiadmin, name="ami prodi bm"),
    path('ami/prodi/<str:pk_test>/FBT/',views.amiprodiadmin, name="ami prodi fbt"),
    path('ami/prodi/<str:pk_test>/PDE/',views.amiprodiadmin, name="ami prodi pde"),
    path('ami/prodi/<str:pk_test>/REE/',views.amiprodiadmin, name="ami prodi ree"),
    path('ami/prodi/<str:pk_test>/CSE/',views.amiprodiadmin, name="ami prodi cse"),

    path('abpt/prodi/<str:pk_test>/DBT/',views.abptprodiadmin, name="abpt prodi dbt"),
    path('abpt/prodi/<str:pk_test>/BM/',views.abptprodiadmin, name="abpt prodi bm"),
    path('abpt/prodi/<str:pk_test>/FBT/',views.abptprodiadmin, name="abpt prodi fbt"),
    path('abpt/prodi/<str:pk_test>/PDE/',views.abptprodiadmin, name="abpt prodi pde"),
    path('abpt/prodi/<str:pk_test>/REE/',views.abptprodiadmin, name="abpt prodi ree"),
    path('abpt/prodi/<str:pk_test>/CSE/',views.abptprodiadmin, name="abpt prodi cse"),

    # ____________ RESET PASSWORD ____________

    path('reset_password/', auth_views.PasswordResetView.as_view(template_name="UPM_Reset_Password.html"), name="reset_password"),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name="UPM_Reset_Password_sent.html"), name="password_reset_done"),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="UPM_Reset_Password_form.html"), name="password_reset_confirm"),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name="UPM_Reset_Password_done.html"), name="password_reset_complete"),


]
    





























































